package com.sda.streams;

import java.util.*;
import java.util.stream.Collectors;

public class Exercise2Main {
    public static void main(String[] args) {
        Person person1 = new Person("Jacek", "Kowalski", 18, true);
        Person person2 = new Person("Jacek", "Górski", 15, true);
        Person person3 = new Person("Andżelika", "Dżoli", 25, false);
        Person person4 = new Person("Wanda", "Ibanda", 12, false);
        Person person5 = new Person("Marek", "Marecki", 17, true);
        Person person6 = new Person("Johny", "Brawo", 25, true);
        Person person7 = new Person("Stary", "Pan", 80, true);
        Person person8 = new Person("Newbie", "Noob", 12, true);
        Person person9 = new Person("Newbies", "Sister", 19, false);

        String linia = "abc;def;gha";
        String[] splits = linia.split(";");
        List<String> listSplits = Arrays.asList(splits);

        List<String> languages1 = Arrays.asList("Java;Cobol;Cpp;Lisp".split(";"));
        List<String> languages2 = Arrays.asList("Java;Lisp".split(";"));
        List<String> languages3 = Arrays.asList("Java;Cobol;Cpp;Lisp;C#".split(";"));
        List<String> languages4 = Arrays.asList("C#;C;Cpp".split(";"));
        List<String> languages5 = Arrays.asList("Java;Assembler;Scala;Cobol".split(";"));
        List<String> languages6 = Arrays.asList("Java;Scala".split(";"));
        List<String> languages7 = Arrays.asList("C#;C".split(";"));
        List<String> languages8 = Collections.emptyList();
        List<String> languages9 = Arrays.asList("Java");


        Programmer programmer1 = new Programmer(person1, languages1);
        Programmer programmer2 = new Programmer(person2, languages2);
        Programmer programmer3 = new Programmer(person3, languages3);
        Programmer programmer4 = new Programmer(person4, languages4);
        Programmer programmer5 = new Programmer(person5, languages5);
        Programmer programmer6 = new Programmer(person6, languages6);
        Programmer programmer7 = new Programmer(person7, languages7);
        Programmer programmer8 = new Programmer(person8, languages8);
        Programmer programmer9 = new Programmer(person9, languages9);

        List<Programmer> programmers = new ArrayList<>(Arrays.asList(
                programmer1, programmer2, programmer3,
                programmer4, programmer5, programmer6,
                programmer7, programmer8, programmer9));

        // uzyskaj listę programistek, które piszą w Javie i Cpp
        List<String> szukaneJezyki = Arrays.asList("Java", "Cpp");

        List<Programmer> programistkiZJavaCpp = programmers.stream()
                .filter(programmer -> !programmer.getPerson().isMezczyzna())
                .filter(programmer -> programmer.getLanguages().containsAll(szukaneJezyki))
                .collect(Collectors.toList());

        ////
        // uzyskaj listę męskich imion
//        List<String> imionaMeskie = programmers.stream()
//                .filter(p-> p.getPerson().isMezczyzna())
//                .map(p-> p.getPerson().getImie())
//                .collect(Collectors.toList());

        // sprawdzamy ostatnia litere imienia, jeśli równa 'a', to imie żeńskie
        List<String> imionaMeskie = programmers.stream()
                .map(p -> p.getPerson().getImie())
                .filter(imie -> imie.endsWith("a"))
                .collect(Collectors.toList());

        // uzyskaj set wszystkich języków opanowanych przez programistów
        List<String> jakiesStringi = new ArrayList<>(Arrays.asList(
                "abc", "def", "gha", "i", "j", "k", "m",
                "gha", "gha", "gha", "j"
        ));
        Set<String> set = new HashSet<>(jakiesStringi);
//        System.out.println(set);

        // jeśli wynikiem ma być set
//        Set<String> languages = programmers.stream()
//                .flatMap(p-> p.getLanguages().stream())
//                .distinct()
//                .collect(Collectors.toSet());

        // jeśli wynikiem miałaby być lista
        List<String> languages = programmers.stream()
                .flatMap(p -> p.getLanguages().stream())
                .distinct()
                .collect(Collectors.toList());


        System.out.println(languages);

        // uzyskaj listę nazwisk programistów, którzy znają więcej, niż dwa języki
        List<String> namesNotNewbies = programmers.stream()
                .filter(p -> p.getLanguages().size() > 2)
                .map(p -> p.getPerson().getNazwisko())
                .collect(Collectors.toList());

        //sprawdź, czy istnieje chociaż jedna osoba, która nie zna żadnego języka
        boolean isFullNewbie = programmers.stream()
                .anyMatch(p -> p.getLanguages().isEmpty());


        //uzyskaj ilość wszystkich języków opanowanych przez programistki
        List<String> languagesWemen = programmers.stream()
                .filter(p -> !p.getPerson().isMezczyzna()) // odfiltrować kobiety
                .flatMap(p -> p.getLanguages().stream())    // wyciągamy kolekcję samych języków
                .distinct()                                 // unikalne jezyki
//                .count();                                 // wyciągamy ilość (zamiast zbierania w kolekcję
                .collect(Collectors.toList());              // zbieramy w kolekcję

        int ilosc = languagesWemen.size();
    }
}
