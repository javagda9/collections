package com.sda.streams;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.Optional;

public class Exercise3Main {
    public static void main(String[] args) {
        Comparator<String> comparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (o1.length() > o2.length()) {
                    return 1;
                } else if (o2.length() > o1.length()) {
                    return -1;
                }
                return 0;
            }
        };

        Optional<String> longestLine = null;
        try (BufferedReader reader = new BufferedReader(new FileReader("input.txt"))) {
            longestLine = reader.lines().max(comparator);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(longestLine.get());
    }
}
