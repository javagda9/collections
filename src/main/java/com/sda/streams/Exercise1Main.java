package com.sda.streams;

import com.sda.streams.optional.Book;
import com.sda.streams.optional.Library;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Exercise1Main {
    public static void main(String[] args) {
        Person person1 = new Person("Jacek", "Kowalski", 18, true);
        Person person2 = new Person("Jacek", "Górski", 15, true);
        Person person3 = new Person("Andżelika", "Dżoli", 25, false);
        Person person4 = new Person("Wanda", "Ibanda", 12, false);
        Person person5 = new Person("Marek", "Marecki", 17, true);
        Person person6 = new Person("Johny", "Brawo", 25, true);
        Person person7 = new Person("Stary", "Pan", 80, true);
        Person person8 = new Person("Newbie", "Noob", 12, true);
        Person person9 = new Person("Newbies", "Sister", 19, false);

        String linia = "abc;def;gha";
        String[] splits = linia.split(";");
        List<String> listSplits = Arrays.asList(splits);

        List<String> languages1 = Arrays.asList("Java;Cobol;Cpp;Lisp".split(";"));
        List<String> languages2 = Arrays.asList("Java;Lisp".split(";"));
        List<String> languages3 = Arrays.asList("Java;Cobol;Cpp;Lisp;C#".split(";"));
        List<String> languages4 = Arrays.asList("C#;C;Cpp".split(";"));
        List<String> languages5 = Arrays.asList("Java;Assembler;Scala;Cobol".split(";"));
        List<String> languages6 = Arrays.asList("Java;Scala".split(";"));
        List<String> languages7 = Arrays.asList("C#;C".split(";"));
        List<String> languages8 = Collections.emptyList();
        List<String> languages9 = Arrays.asList("Java");


        Programmer programmer1 = new Programmer(person1, languages1);
        Programmer programmer2 = new Programmer(person2, languages2);
        Programmer programmer3 = new Programmer(person3, languages3);
        Programmer programmer4 = new Programmer(person4, languages4);
        Programmer programmer5 = new Programmer(person5, languages5);
        Programmer programmer6 = new Programmer(person6, languages6);
        Programmer programmer7 = new Programmer(person7, languages7);
        Programmer programmer8 = new Programmer(person8, languages8);
        Programmer programmer9 = new Programmer(person9, languages9);

        List<Programmer> programmers = new ArrayList<>();
//        programmers.add(programmer1);
//        programmers.add(programmer2);
//        programmers.add(programmer3);
//        programmers.add(programmer4);
//        programmers.add(programmer5);
//        programmers.add(programmer6);
//        programmers.add(programmer7);
//        programmers.add(programmer8);
//        programmers.add(programmer9);

        // ^^ to samo co u góry, ale inaczej
        programmers = new ArrayList<>(Arrays.asList(
                programmer1,
                programmer2,
                programmer3,
                programmer4,
                programmer5,
                programmer6,
                programmer7,
                programmer8,
                programmer9));

        // Sposób 1 - przez wykluczanie/usuwanie:
        List<Programmer> kopia = new ArrayList<>(programmers);
        for (Programmer programmer : programmers) {
            if (!programmer.getPerson().isMezczyzna()) {
                // jeśli nie jest mężczyzną to usuwamy z kopii
                kopia.remove(programmer);
            }
        }

        // Sposób 2 - przez dodawanie
        List<Programmer> kopia2 = new ArrayList<>();
        for (Programmer programmer : programmers) {
            if (programmer.getPerson().isMezczyzna()) {
                // jeśli jest mężczyzną to dodaje go do kopii2
                kopia2.add(programmer);
            }
        }


        // stworzenie anonimowe
//        Predicate<Programmer> predicate = new Predicate<Programmer>() {
//            @Override
//            public boolean test(Programmer XXProgrammer) {
//                String imie = XXProgrammer.getPerson().getImie();
//                boolean isMezczyzna = XXProgrammer.getPerson().isMezczyzna();
//
//                System.out.println(imie + " jest " + isMezczyzna);
//
//                // jeśli true to zostanie zaliczony do kolekcji końcowej
//                return isMezczyzna;
//            }
//        };

        //
//        Predicate<Programmer> predicate = new MaleProgrammerPredicate();
//        Predicate<Programmer> predicate = XXProgrammer -> XXProgrammer.getPerson().isMezczyzna();
//        Predicate<Programmer> predicate = XXProgrammer -> {
//            String imie = XXProgrammer.getPerson().getImie();
//            boolean isMezczyzna = XXProgrammer.getPerson().isMezczyzna();
//
//            System.out.println(imie + " jest " + isMezczyzna);
//            return XXProgrammer.getPerson().isMezczyzna();
//        };


        // Zadanie A
        List<Programmer> wynik = programmers.stream()
                .filter(XXProgrammer -> XXProgrammer.getPerson().isMezczyzna())
                .collect(Collectors.toList());

        System.out.println(wynik);
        wynik.add(new Programmer(null, null));

//        Zadanie B
        List<Programmer> pelnoletnieKobiety = programmers.stream()
                // poniższa linia zamiast dwóch filtrów
//                .filter(XXProgrammer -> !XXProgrammer.getPerson().isMezczyzna() && XXProgrammer.getPerson().getWiek() >= 18)
                .filter(XXProgrammer -> !XXProgrammer.getPerson().isMezczyzna())
                .filter(XXProgrammer -> XXProgrammer.getPerson().getWiek() >= 18)
                .collect(Collectors.toList());


        // Zadanie C
        Optional<Programmer> optionalJacek = programmers.stream()
                .filter(programmer -> programmer.getPerson().getImie().equalsIgnoreCase("jacek"))
                .findFirst();

        System.out.println();
        System.out.println();
        // wypisz jacka tylko jeśli został zwrócony (wypisz obiekt PROGRAMMER - toString na programmer)
        optionalJacek.ifPresent(jacek -> System.out.println(jacek));


        System.out.println();
        System.out.println();
        // wypisz optional (niezależnie czy zawiera wartość) - toString na Optional
        System.out.println(optionalJacek);


        // Zadanie C
//        Function<Programmer, Person> mapper = new Function<Programmer, Person>() {
//            @Override
//            public Person apply(Programmer programmer) {
//                return programmer.getPerson();
//            }
//        };
//        Optional<Person> optionalPersonJacek = programmers.stream()
//                .map(mapper)
//                .filter(programmer -> programmer.getPerson().getImie().equalsIgnoreCase("jacek"))
//                .findFirst();

        // *********************
        Optional<Person> optionalPersonJacek = programmers.stream()
                .map(programmer -> programmer.getPerson())  // mapowanie, zwraca stream Person'ów
                .filter(person -> person.getImie().equalsIgnoreCase("jacek")) // z personów filtruje jacka
                .findFirst();

        // listę wszystkich nazwisk osób, które są w przedziale wiekowym: 15-19
        List<String> nazwiska = programmers.stream()
                // filtruję wiek 15-19
                .filter(programmer -> programmer.getPerson().getWiek() >= 15 && programmer.getPerson().getWiek() <= 19)
                // zamieniam każdego programistę na nazwisko
                .map(programmer -> programmer.getPerson().getNazwisko())
                // zbieram wynik w liście
                .collect(Collectors.toList());

        // Zadanie E
        int suma = programmers.stream()
                .mapToInt(programmer -> programmer.getPerson().getWiek())
                .sum();

        // Zadanie F
        OptionalDouble avg = programmers.stream()
                .mapToDouble(programmer -> programmer.getPerson().getWiek())
                .average();

//        Double wartość = 1.0;
//        Double druga = 0.0;
//
////        Double trzecia = wartość/druga;
//        int trzecia = wartość.intValue()/druga.intValue();
//
//        System.out.println(trzecia);

        OptionalDouble avgMan = programmers.stream()
                .filter(programmer -> programmer.getPerson().isMezczyzna())
                .mapToInt(programmer -> programmer.getPerson().getWiek())
                .average();

        Double d = avgMan.getAsDouble();

        // Zadanie G

        Comparator<Programmer> programmerComparator = new Comparator<Programmer>() {
            @Override
            // -1 o1>o2
            // 0 o1==o2
            // 1 o2>o1
            public int compare(Programmer o1, Programmer o2) {
                if (o1.getPerson().getWiek() > o2.getPerson().getWiek()) {
                    return -1;
                } else if (o1.getPerson().getWiek() == o2.getPerson().getWiek()) {
                    return 0;
                }
                return 1;
            }
        };

        Comparator<Person> personComparator = new Comparator<Person>() {
            @Override
            // -1 o1>o2
            // 0 o1==o2
            // 1 o2>o1
            public int compare(Person o1, Person o2) {
                if (o1.getWiek() > o2.getWiek()) {
                    return -1;
                } else if (o1.getWiek() == o2.getWiek()) {
                    return 0;
                }
                return 1;
            }
        };

//        List<Person> programmersLst = programmers.stream()
//                .map(programmer -> programmer.getPerson())
//                .sorted(personComparator).collect(Collectors.toList());
//        System.out.println(programmersLst);


//        Optional<Person> najstarsza = programmers.stream()
//                .sorted(programmerComparator)
//                .map(programmer -> programmer.getPerson())
//                .findFirst();

        // to samo z wyrazeniem lambda
//        Optional<Person> najstarsza = programmers.stream()
//                .sorted((o1, o2) -> {
//                    if (o1.getPerson().getWiek() > o2.getPerson().getWiek()) {
//                        return -1;
//                    } else if (o1.getPerson().getWiek() == o2.getPerson().getWiek()) {
//                        return 0;
//                    }
//                    return 1;
//                })
//                .map(programmer -> programmer.getPerson())
//                .findFirst();
        // to samo z wyrazeniem lambda na person
//        Optional<Person> najstarsza = programmers.stream()
//                .map(programmer -> programmer.getPerson())
//                .sorted((o1, o2) -> {
//                    if (o1.getWiek() > o2.getWiek()) {
//                        return -1;
//                    } else if (o1.getWiek() == o2.getWiek()) {
//                        return 0;
//                    }
//                    return 1;
//                })
//                .findFirst();

        // jeszcze krótszy zapis lambda komparatora
//        Optional<Person> najstarsza = programmers.stream()
//                .map(programmer -> programmer.getPerson())
//                // -1 o1>o2
//                // 0 o1==o2
//                // 1 o2>o1
//                .sorted((o1, o2) -> o1.getWiek() > o2.getWiek() ? -1 : (o2.getWiek() > o1.getWiek() ? 1 : 0))
//                .findFirst();

        Optional<Person> najstarsza = programmers.stream()
                .map(programmer -> programmer.getPerson())
                .max(personComparator);

        System.out.println(najstarsza.get());
    }

}
